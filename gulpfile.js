"use strict";

var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('scss', function () {
    return gulp.src('./training_core/cartridge/scss/default/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('./training_core/cartridge/static/default/css/'));
});

gulp.task('default', function () {
    gulp.watch('./training_core/cartridge/scss/default/**/*.scss', gulp.series('scss'));
});